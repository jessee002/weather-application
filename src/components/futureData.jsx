import React, { Component, useEffect, useState } from "react";
import "../components/weather.css";

function FutureData({ sendData }) {
  if (!sendData) {
    return <div>Loading</div>;
  }
  
  return sendData.list.map((eachDay, index) => {
    return <div className="future-data-bar" key={index}>
      <p>{eachDay.dt_txt}</p>
      <div className="image-weather">
        <img
          src={`https://openweathermap.org/img/wn/${sendData.list[0].weather[0].icon}@2x.png`}
          alt="weather"
        />
        <div className="weather-mode">{eachDay.weather[0].main}</div>
      </div>
      <span className="temperature">{Math.floor(eachDay.main.temp-273.15)+""+"°C"}</span>
    </div>;
  });
}
export default FutureData;
