import React, { Component, useEffect, useState } from "react";
import "../components/weather.css";

function DisplayBar({ sendData }) {

  if(!sendData){
    return(
      <div>Loading</div>
    )
  }
console.log(sendData);
  return (
    <div className="display-box">
      <img src={`https://openweathermap.org/img/wn/${sendData.list[0].weather[0].icon}@2x.png`} alt="" />
      <div className="city-details">
        <div className="city-name">{sendData.city.name}</div>
        <div className="temperature">{Math.floor(sendData.list[0].main.temp - 273.15)+""+"°C"}</div>
        <div className="weather-type">{sendData.list[0].clouds.all}</div>
      </div>
    </div>
  );
}
export default DisplayBar;
