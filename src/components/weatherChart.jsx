import React, { Component, useEffect, useState } from "react";
import { Box, Input } from "@chakra-ui/react";
import { Button } from "@chakra-ui/react";
import "../components/weather.css";
import DisplayBar from "./displayBar";
import WindHumidityBar from "./windHumidityBar";
import FutureData from "./futureData";

function WeatherChart() {
  const [weatherData, setWeatherData] = useState(null);
  const [cityLocation, setCityLocation] = useState("");
  const [cityName, setCityName] = useState("");

  const fetchingData = async () => {
    const res = await fetch(
      `https://api.openweathermap.org/data/2.5/forecast?q=${cityName}&appid=10cd0b66f76e308bc422e9e3dcbc1832`
    );
    res
      .json()
      .then((data) => {
        setWeatherData(data);
      })
      .catch((err) => {
        console.log(Loading);
      });
  };

  function inputCity(e) {
    setCityName(e.target.value);
  }

  return (
    <Box className="container">
      <Box className="search">
        <Input placeholder="Location-name" onChange={inputCity} />
        <Button onClick={fetchingData} colorScheme="teal" variant="solid">
          Search
        </Button>
      </Box>
      <DisplayBar sendData={weatherData} />
      <WindHumidityBar sendData={weatherData} />
      <FutureData sendData={weatherData} />
    </Box>
  );
}
export default WeatherChart;
