import React, { Component, useEffect, useState } from "react";
import "../components/weather.css";
import blackcloud from "../images/blackcloud.gif";
import humidity from "../images/humidity.gif";
import wind from "../images/wind.gif";

function WindHumidityBar({ sendData }) {

  if(!sendData){
    return(
      <div>Loading</div>
    )
  }
       
  return (
    <div className="wind-humidity">
      <div className="stats wind">
        <img src={wind} alt="wind-blowing" />
        <p >{sendData.list[0].wind.speed+" "+"Km/h"}</p>
        <span className="data">wind</span>
      </div>
      <div className="stats humidity">
        <img src={humidity} alt="" />
        <p >{sendData.list[0].wind.speed}</p>
        <span className="data">humidity</span>
      </div>
      <div className="stats cloud-cover">
        <img src={blackcloud} alt="" />
        <p >{sendData.list[0].clouds.all+" "+ "%"}</p>
        <span className="data">cloud-cover</span>
      </div>
    </div>
  );
}
export default WindHumidityBar;
