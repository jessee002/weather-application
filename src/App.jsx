import * as React from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import { ChakraProvider } from "@chakra-ui/react";
import "./App.css";
import { useState } from "react";
import WeatherChart from "./components/weatherChart";

function App() {
  return (
      <ChakraProvider>
        <WeatherChart />
      </ChakraProvider>
  );
}
export default App;